<?php

$action = $_GET['action'];
$file = $_GET['file'];

switch ( $action ) {

	case 'getFiles':
		// Looking for a list of files in a directory
		// Open a file handle.
		if ($handle = opendir('./xml')) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					$return[] = $entry;
				}
			}
			closedir($handle);
		}
		echo json_encode($return);
		break;

	case 'getHeaders':
		if ( $file == null or $file == "" ) {
			// They didn't specify a file, so we return an eror and break
			$return = array();
			$return['status'] = "error";
			$return['message'] = "File requested but no file name specified.";
		}
		else {
			$xml = simplexml_load_file('./xml/'.$file);

			foreach($xml->children() as $child) {

				$return[] = $child->getName();

				foreach ($child as $key => $value) {
					$return[] = $value->getName();
				}
			
				break;
			}
		}
		echo json_encode($return);
		break;

	case 'getFile':
		// We retrieve an XML file, encode to JSON and send this back
		if ( $file == null or $file == "" ) {
			// They didn't specify a file, so we return an eror and break
			$return = array();
			$return['status'] = "error";
			$return['message'] = "File requested but no file name specified.";
		}
		else {
			// Otherwise, we open the file, and get it ready to be manipulated
			$xml = simplexml_load_file("./xml/".$file);

			foreach($xml->children() as $child) {

				foreach ($child as $key => $value) {
					$content[] =  (string) $value;
				}
		
				$return[] = $content;
				unset($content);

			}

			echo json_encode($return);
			break;
		}

	case 'submitData':
		// We retrieve an XML file, and check it exists
		if ( $file == null or $file == "" ) {
			// They didn't specify a file, so we return an eror and break
			$return = array();
			$return['status'] = "error";
			$return['message'] = "File requested but no file name specified.";
		}
		else {
			// Otherwise, we open the file, and get it ready to be manipulated
			$xml = simplexml_load_file("./xml/".$file);
			$c = 0;
			// Get the name of the child element we'll be making!
			foreach ($xml->children() as $child) {
				if ( $c > 0 ) {
					$childElement = $child->getName();
				}
				else {
					$c++;
				}
			}

			// First task, add a child
			$modifiedXML = $xml->addChild($childElement);

			// We need to unset the filename attribute
			unset( $_POST['filename'] );

			// Now we have it, process the POST data
			foreach($_POST as $name => $value)  {
				$modifiedXML->addChild($name, $value);
			}

			// Open a file handle
			$handle = fopen("./xml/".$file,"w+");
			// Write the data
			fwrite($handle, $xml->asXML());
			fclose($handle);

			$return = array();
			$return['status'] = "success";
			$return['message'] = "File changed successfully.";
		}

		echo json_encode($return);
		break;


	default:
		$return = array();
		$return['status'] = "error";
		$return['message'] = "The action specified was not recognised.";

		echo json_encode($return);
}