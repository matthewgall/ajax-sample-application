/**
 * We'll only modify the page once the DOM is ready for manipulation
 **/
$(document).ready(function() {
	
	/**
	 * This function will be executed on page load, upon load, fire off a request to
	 * api/?action=getFiles. This will get a JSON response
	 **/
	// Hide some elements while they are prepared...
	$('#xmlOpen').hide();
	$('#xmlSearch').hide();
	$('#xmlAdd').hide();

	// The document is ready to be manipulated, so we trigger an AJAX call, lucky jQuery has a getJSON function.
	$.getJSON("/api/index.php?action=getFiles", function(json) {
		// Now, this data has been parsed by getJSON, so we can be sure this stuff is fine!
		// Next job, iterate through it and build a select statement
		var render = "";
		var form = "";
		// Now we turn JSON into a string
		var jsonString = JSON.stringify( json );
		var xmlFiles = jsonString.split(',');

		// We iterate through and append our values
		for (var k = 0; k < (xmlFiles.length); k++)	{
			xmlFiles[k] = xmlFiles[k].replace('\"','');
			xmlFiles[k] = xmlFiles[k].replace('\"','');
			xmlFiles[k] = xmlFiles[k].replace(']','');
			xmlFiles[k] = xmlFiles[k].replace('[','');
			render += '<option value=\"' + xmlFiles[k] + '\">' + xmlFiles[k] + '</option>';
		} // Close for loop

		// Now we need the closing tag
		render += "</select>";
		// And append this to the span within the page
		$("#fileSelect").append(render);
	}); // Close getJSON (getFiles)

	/**
	 * This event should fire when the select box (select#fileSelect) is changed
	 * API call made to /api/?action=getFile&file={whatever}
	 **/

	 $('#fileSelect').change(function () {
	 	// Right, now it has changed, we detect the value
	 	var fileSelected = $("#fileSelect").val();
	 	render = "";
	 	form = "";

	 	if ( fileSelected == "0" ) {
	 		// Nothing happens here, we don't want it to do anything with the default element
	 		$('#xmlOpen').hide();
	 		$('#xmlSearch').hide();
	 		$('#xmlAdd').hide();
	 	}
	 	else {
	 		// AJAX request time!
	 		// First, we get the headers of the file
	 		$.getJSON("/api/index.php?action=getHeaders&file="+fileSelected, function(headingData) {

	 			render = "<table id=\"xmlTable\"><tr>";
	 			// They are stored in response, so we'll split them up (they have int id's) and make a row just for them!
				for (var i = 1; i <(headingData.length); i++) {			
					render += "<td class='tblHeading'><strong>" + headingData[i].toUpperCase() + "</strong></td>";
					form += "<label for=\"" + headingData[i] + "\">" + headingData[i] + "</label><input type=\"text\" name=\"" + headingData[i] + "\"><br />";
				} // Close for loop

				// Then we add a hidden field for the name of the file
				form += "<input type=\"hidden\" name=\"filename\" value=\"" + fileSelected + "\">";

				$.getJSON("/api/index.php?action=getFile&file="+fileSelected, function(contentData) {

					numItems = contentData.length;

					//Nested for loop which opens the child element checks how many attributes it contains and adds a row to the table 
					for (var j = 0; j < numItems; j++) {				
						render += "<tr>";
						for (var attributes = 0; attributes < contentData[j].length; attributes++) {
							render += "<td class='tblContent'>" + contentData[j][attributes] + "</td>";
						} // Close for loop
						render += "</tr>";						
					} // Close for loop

					render += "</tr></table>";
		 			render += "</table>";

		 			// Now we have it all ready, we can show our prepared areas!
		 			$('#xmlOpen').show();
		 			$('#xmlSearch').show();
		 			$('#xmlAdd').show();

		 			$("#xmlOpen span").empty();
		 			$("#xmlOpen span").append(render);
		 			$("#xmlAdd span").empty();
		 			$("#xmlAdd span").append(form);
				}); // Close getJSON (getFile)
		 	}); // Close getJSON (getHeaders)
	 	} // Close else{} statement
	 }); // Close #fileSelect change listener

	/**
	 * This event should fire when the search button is called, standard processing rules apply
	 * API call made to /api/?action=getFile&file={whatever}
	 **/
	$('#btnSearch').click(function () {
		
		var fileSelected = $("#fileSelect").val();
	 	render = "";
	 	form = "";

		// So they have clicked the search button, first task, have they entered a term?
		if ( $('#searchTerm').val() == "" ) {
			// Whoopsie, they haven't entered anything. Bit silly.
			alert("You didn't enter a search term. Please check and try again!");
		}
		else {
			// They entered a search term, so now time to process the search in the current XML file...
			$.getJSON("/api/index.php?action=getHeaders&file="+fileSelected, function(headingData) {

	 			render = "<table id=\"xmlTable\"><tr>";
	 			// They are stored in response, so we'll split them up (they have int id's) and make a row just for them!
				for (var i = 1; i <(headingData.length); i++) {			
					render += "<td class='tblHeading'><strong>" + headingData[i].toUpperCase() + "</strong></td>";
				}

				$.getJSON("/api/index.php?action=getFile&file="+fileSelected, function(contentData) {

					numItems = contentData.length;

					//Nested for loop which opens the child element checks how many attributes it contains and adds a row to the table 
					for (var j = 0; j < numItems; j++) {				
						for (var attributes = 0; attributes < contentData[j].length; attributes++) {
							if (contentData[j][attributes] == $('#searchTerm').val()) {
								render += "<tr>";
								for (var attributes = 0; attributes < contentData[j].length; attributes++) {
									render += "<td class='tblContent'>" + contentData[j][attributes] + "</td>";
								} // Close for loop
								render += "</tr>";
								attributes = contentData[j].length - 1;	
							} // Close if statement
						} // Close for loop				
					} // Close for loop

					render += "</tr></table>";
					$("#xmlOpen span").empty();
		 			$("#xmlOpen span").append(render);

				}); // Close getJSON(getFile)
			}); // Close getJSON (getHeaders)
		} // Close else{} statement
	}); // Close $('#btnSearch') click listener

	/**
	 * This event should fire when the search reset button is called, standard processing rules apply
	 * API call made to /api/?action=getHeaders&file={whatever} & /api/?action=getFile&file={whatever}
	 **/
	$('#btnAdd').click(function () {
		// So first, we need to get the name of the file we are modifying
		var fileSelected = $("#fileSelect").val();

		// And then, we need to get a list of all the form input's and their values
		var submitValues = $('#addForm').serialize();

		// So now we have the data, we're going to do an AJAX post request to ?action=submitData&file={whatever}
		$.ajax({
			type: "POST",
			url: "api/index.php?action=submitData&file="+fileSelected,
			data: submitValues
		}).done(function( msg ) {
			alert( "Data Saved: " + msg );
		});

	}); //Close $('#btnAdd') click listener

	$('#btnSearchReset').click( function() {
		// Clear some variables
		var fileSelected = $("#fileSelect").val();
	 	render = "";

	 	$.getJSON("/api/index.php?action=getHeaders&file="+fileSelected, function(headingData) {

	 		render = "<table id=\"xmlTable\"><tr>";
	 		// They are stored in response, so we'll split them up (they have int id's) and make a row just for them!
			for (var i = 1; i <(headingData.length); i++) {			
				render += "<td class='tblHeading'><strong>" + headingData[i].toUpperCase() + "</strong></td>";
			} // Close for loop

			$.getJSON("/api/index.php?action=getFile&file="+fileSelected, function(contentData) {

				numItems = contentData.length;

				//Nested for loop which opens the child element checks how many attributes it contains and adds a row to the table 
				for (var j = 0; j < numItems; j++) {				
					render += "<tr>";
					for (var attributes = 0; attributes < contentData[j].length; attributes++) {
						render += "<td class='tblContent'>" + contentData[j][attributes] + "</td>";
					} // Close for loop
					render += "</tr>";						
				} // Close for loop

				render += "</tr></table>";
	 			render += "</table>";

	 			$("#xmlOpen span").empty();
	 			$("#xmlOpen span").append(render);
			}); // Close getJSON (getFile)
		}); // Close getJSON (getHeaders)
	}) // Close $('#btnSearchReset') click listener
 }); // Close $(document).ready handler